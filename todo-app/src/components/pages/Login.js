import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";
import axios from "axios";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  async onSubmit(e) {
    try {
      e.preventDefault();
      const { email, password } = this.state;
      const userLogin = {
        email,
        password
      };
      const response = await axios.post(
        "http://localhost:3001/users/login",
        userLogin
      );
      localStorage.setItem("jwtToken", response.data.token);
      if (response.status === 200) {
        this.props.history.push("/home");
      }

      this.setState({
        email: "",
        password: ""
      });
    } catch {
      alert("Email or Password incorrect");
    }
  }
  render() {
    return (
      <div className="container" style={{ marginTop: 10 }}>
        <h3>Login to Todo App</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Email: </label>
            <input
              name="email"
              type="email"
              className="form-control"
              onChange={this.onChangeEmail}
              required
            />
          </div>
          <div className="form-group">
            <label>Password: </label>
            <input
              name="password"
              type="password"
              className="form-control"
              onChange={this.onChangePassword}
              required
            />
          </div>
          <div className="form-group">
            <input type="submit" value="Login" className="btn btn-primary" />
          </div>
          <br />
          <div className="form-group">
            <Link to="/register">
              <input
                type="submit"
                value="Register"
                className="btn btn-primary"
              />
            </Link>
          </div>
        </form>
      </div>
    );
  }
}