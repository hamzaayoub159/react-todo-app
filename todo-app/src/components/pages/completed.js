import React, { Component } from "react";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import AppHeader from "../common/AppHeader";
import axios from "axios";

const Todo = props => (
  <tr>
    <td>{props.todo.description}</td>
    <td>{props.todo.completed}</td>
  
    <td>
      <Link to={"/delete/" + props.todo._id}>Delete</Link>
    </td>
  </tr>
);

class completed extends Component {
  constructor(props) {
    super(props);
    this.state = { todos: [] };
    this.onLogout = this.onLogout.bind(this);
  }

  onLogout(e) {
    e.preventDefault();
    localStorage.removeItem("jwtToken");
    this.props.history.push("/");
  }

  async componentDidMount() {
    const token = localStorage.getItem("jwtToken");
    try {
      const res = await axios.get("http://localhost:3001/todos?completed=true", {
        headers: { Authorization: `Bearer ${token}` }
      });
      console.log(res.data);
      this.setState({ todos: res.data });
    } catch {
      alert("Error occur");
    }
  }
  todoList() {
    return this.state.todos.map(function(currentTodo, i) {
      return <Todo todo={currentTodo} key={i} />;
    });
  }
  render() {
    return (
      <div className="container">
        <AppHeader />
        <h3>Completed Task</h3>
        <form>
          <div className="form-group">
            <input
              type="submit"
              value="Logout"
              className="btn btn-primary"
              onClick={this.onLogout}
            />
          </div>
        </form>
        <table className="table table-striped" style={{ marginTop: 20 }}>
          <thead>
            <tr>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>{this.todoList()}</tbody>
        </table>
      </div>
    );
  }
}
export default completed;