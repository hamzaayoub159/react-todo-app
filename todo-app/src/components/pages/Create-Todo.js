import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import AppHeader from "../common/AppHeader";

export default class CreateTodo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: "",
      completed: false
    };
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeCompleted = this.onChangeCompleted.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeDescription(e) {
    this.setState({
      description: e.target.value
    });
  }

  onChangeCompleted(e) {
    this.setState({
      completed: e.target.value
    });
  }

  async onSubmit(e) {
    try {
      e.preventDefault();
      const { description, completed } = this.state;
      const token = localStorage.getItem("jwtToken");
      const newTodo = {
        description,
        completed
      };

      const res = await axios.post("http://localhost:3001/todos", newTodo, {
        headers: { Authorization: `Bearer ${token}` }
      });
      if (res.status === 201) {
        alert("Todo Created Successfully");
        this.props.history.push("/home");
      }

      this.setState({
        description: "",
        completed: false
      });
    } catch {
      alert("Todo Can not create");
    }
  }

  render() {
    const { description } = this.state;
    return (
      <div className="container" style={{ marginTop: 10 }}>
        <AppHeader />
        <h3>Create New Todo</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Description:</label>
            <textarea
              className="form-control"
              rows="5"
              value={description}
              onChange={this.onChangeDescription}
            ></textarea>
          </div>
          <div className="form-group">
            <input
              type="submit"
              value="Create Todo"
              className="btn btn-primary"
            />
          </div>
        </form>
      </div>
    );
  }
}