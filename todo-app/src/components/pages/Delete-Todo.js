import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

export default class DeleteTodo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: ""
    };
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }
  async componentDidMount() {
    try {
      const token = localStorage.getItem("jwtToken");
      const res = await axios.get(
        "http://localhost:3001/todos/" + this.props.match.params.id,
        {
          headers: { Authorization: `Bearer ${token}` }
        }
      );
      this.setState({
        description: res.data.description,
        completed: res.data.completed
      });
    } catch (e) {
      alert(e);
    }
  }
  onChangeDescription(e) {
    this.setState({
      description: e.target.value
    });
  }

  async onDelete(e) {
    e.preventDefault();
    const token = localStorage.getItem("jwtToken");
    const res = await axios.delete(
      "http://localhost:3001/todos/" + this.props.match.params.id,
      {
        headers: { Authorization: `Bearer ${token}` }
      }
    );
    this.setState({
      description: res.data.description,
      completed: res.data.completed
    });
    if (res.status === 200) {
      alert("Data Deleted Successfully");
      this.props.history.push("/home");
    }
  }

  render() {
    return (
      <div className="container">
        <h3 align="center">Update Todo</h3>
        <form onSubmit={this.onDelete}>
          <div className="form-group">
            <label>Description: </label>
            <textarea
              className="form-control"
              rows="5"
              value={this.state.description}
              onChange={this.onChangeDescription}
            ></textarea>
          </div>

          <br />

          <div className="form-group">
            <input
              type="submit"
              value="Delete Todo"
              className="btn btn-danger"
            />
          </div>

          <br />
        </form>
        <div className="form-group">
          <Link to="/home">
            <input
              type="submit"
              value="Back"
              float="right"
              className="btn btn-primary"
            />
          </Link>
        </div>
      </div>
    );
  }
}