import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Register from "../pages/Register";
import Login from "../pages/Login";
import TodosList from "../pages/Todo-List";
import Create from "../pages/Create-Todo";
import EditTodo from "../pages/Edit-Todo";
import DeleteTodo from "../pages/Delete-Todo";
import completed from "../pages/completed";

class App extends Component {
render() {
return (
<Router>
<Switch>
<Route exact path="/" component={Login} />
<Route path="/register" component={Register} />
<Route path="/home" component={TodosList} />
<Route path="/create" component={Create} />
<Route path="/edit/:id" component={EditTodo} />
<Route path="/delete/:id" component={DeleteTodo} />
<Route path="/complete" component={completed} />

</Switch>
</Router>
);
}
}

export default App;